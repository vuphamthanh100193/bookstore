import 'reflect-metadata';
import App from './app';
// import BookController from './controllers/book.controller';
// import { myContainer } from './config/inversify';
import { CONFIG } from './src/config';

// let controller = myContainer.resolve(BookController);


const app = new App(CONFIG.PORT , CONFIG.DB_STRING);

// app.listen();
