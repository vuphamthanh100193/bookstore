"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = __importDefault(require("express"));
var inversify_1 = require("inversify");
var inversify_express_utils_1 = require("inversify-express-utils");
require("./src/controller/bookController");
var bodyParser = __importStar(require("body-parser"));
var db_client_1 = require("./src/db/db_client");
var cors_1 = __importDefault(require("cors"));
require("reflect-metadata");
require("./src/controller/bookController");
var bookService_1 = require("./src/services/bookService");
var authenticate_1 = require("./src/middlewares/authenticate");
var App = /** @class */ (function () {
    function App(port, db_string) {
        this.app = express_1.default();
        this.port = port;
        this.db_string = db_string;
        this.initializeMiddleware();
        this.connectToDB();
        this.listen();
    }
    App.prototype.initializeMiddleware = function () {
        this.app.use(cors_1.default());
    };
    App.prototype.connectToDB = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, db_client_1.getDatabaseClient(this.db_string)];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    App.prototype.getToken = function (app) {
        app.post('/getToken', function (req, res) {
            if (req.body.username === "admin") {
                if (req.body.password === 123) {
                    //if eveything is okey let's create our token 
                    var token = authenticate_1.createdToken();
                    res.json({
                        message: 'authentication done ',
                        token: token
                    });
                }
                else {
                    res.json({ message: "please check your password !" });
                }
            }
            else {
                res.json({ message: "user not found !" });
            }
        });
    };
    App.prototype.listen = function () {
        var _this = this;
        // set up container
        var container = new inversify_1.Container();
        // set up bindings
        container.bind('BookService').to(bookService_1.BookService);
        // create server
        var server = new inversify_express_utils_1.InversifyExpressServer(container);
        server.setConfig(function (app) {
            // add body parser
            app.use(bodyParser.urlencoded({
                extended: true
            }));
            app.use(bodyParser.json());
            app.use('/api', function (req, res, next) { return authenticate_1.authenticate(req, res, next); });
        });
        var app = server.build();
        this.getToken(app);
        app.listen(this.port, function () {
            console.info("App is running on port " + _this.port);
        });
    };
    return App;
}());
exports.default = App;
