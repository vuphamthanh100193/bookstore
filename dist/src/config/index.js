"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
require("dotenv/config");
exports.CONFIG = {
    PORT: parseInt("" + process.env.PORT, 10) || 4000,
    DB_STRING: process.env.DB_STRING,
};
