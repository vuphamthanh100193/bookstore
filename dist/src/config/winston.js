"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var app_root_path_1 = __importDefault(require("app-root-path"));
var winston = require('winston');
var winstonDaily = require('winston-daily-rotate-file');
// define the custom settings for each transport (file, console)
var options = {
    file: {
        level: 'info',
        filename: app_root_path_1.default + "/logs/app.log",
        handleExceptions: true,
        json: true,
        maxsize: 5242880,
        maxFiles: 5,
        colorize: false,
    },
    console: {
        level: 'debug',
        handleExceptions: true,
        json: false,
        colorize: true,
    },
};
var transport = new (winston.transports.DailyRotateFile)({
    filename: app_root_path_1.default + "/logs/%DATE%-error.log",
    datePattern: 'YYYY-MM-DD',
    maxSize: '20m',
    maxFiles: '14d',
});
// instantiate a new Winston Logger with the settings defined above
var logger = new winston.createLogger({
    format: winston.format.combine(winston.format.colorize(), winston.format.timestamp({
        format: 'YYYY-MM-DD HH:mm:ss',
    }), winston.format.prettyPrint(), winston.format.printf(function (info) {
        return info.level + "  " + info.timestamp + "  " + info.message + " ";
    })),
    // transports: [
    //     new winston.transports.File(options.file),
    //     new winston.transports.Console(options.console)
    // ],
    transports: [
        transport,
        new winston.transports.Console(options.console)
    ],
    exitOnError: false
});
// create a stream object with a 'write' function that will be used by `morgan`
logger.stream = {
    write: function (message, encoding) {
        // use the 'info' log level so the output will be picked up by both transports (file and console)
        logger.info(message);
    },
};
exports.default = logger;
