"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose = __importStar(require("mongoose"));
var bookSchema = new mongoose.Schema({
    title: {
        type: String,
        required: true,
        maxlength: 30,
        unique: true,
    },
    category: {
        type: String,
        enum: ['drama', 'comedy', 'sport'],
        required: true,
    },
    description: {
        type: String,
        required: true
    }
});
var Book = mongoose.model("Book", bookSchema);
exports.default = Book;
