"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
function authenticate(req, res, next) {
    // check header for the token
    var token = req.headers['access-token'];
    // decode token
    if (token) {
        // verifies secret and checks if the token is expired
        try {
            var decoded = jsonwebtoken_1.default.verify(token, 'heymynameismohamedaymen');
            next();
        }
        catch (err) {
            return res.json({ message: 'invalid token' });
        }
    }
    else {
        // if there is no token  
        res.send({
            message: 'No token provided.'
        });
    }
}
exports.authenticate = authenticate;
function createdToken() {
    var payload = {
        check: true
    };
    return jsonwebtoken_1.default.sign(payload, 'heymynameismohamedaymen', {
        expiresIn: 1440 // expires in 24 hours
    });
}
exports.createdToken = createdToken;
