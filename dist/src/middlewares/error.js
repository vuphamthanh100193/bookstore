"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function handleError(error, response) {
    var statusCode = error.statusCode;
    var errMsg = '';
    if (error.message.errmsg) {
        errMsg = error.message.errmsg;
    }
    else
        errMsg = error.message;
    response.send({
        result: statusCode,
        message: errMsg
    });
}
exports.handleError = handleError;
