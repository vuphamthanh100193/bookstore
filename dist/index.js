"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
require("reflect-metadata");
var app_1 = __importDefault(require("./app"));
// import BookController from './controllers/book.controller';
// import { myContainer } from './config/inversify';
var config_1 = require("./src/config");
// let controller = myContainer.resolve(BookController);
var app = new app_1.default(config_1.CONFIG.PORT, config_1.CONFIG.DB_STRING);
// app.listen();
