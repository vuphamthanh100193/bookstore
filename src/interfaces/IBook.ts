export interface IBook {
    title: String,
    category: String,
    description: String
}
