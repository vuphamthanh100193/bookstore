import mongoose from "mongoose";
import winston from "../config/winston";
import appRoot from 'app-root-path';

export type DbClient = mongoose.Mongoose;


export async function getDatabaseClient(db_string: string) {
        try {
            await mongoose.connect(db_string, { useUnifiedTopology: true });
            winston.info(`Db conenction success: ${db_string}`);
          } catch (error) {      
            winston.error(` [${__filename}] Db conenction error: ${error}`);
          }
}
