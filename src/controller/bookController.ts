import * as express from 'express';
import { IBookController } from '../interfaces/IBookController';
import { injectable, inject  } from 'inversify';
import { interfaces, controller, httpGet, httpPost, request, response, httpPut, httpDelete } from "inversify-express-utils";
import { BookService } from "../services/bookService";
import { handleError } from "../middlewares/error";
import { senToClient } from "../middlewares/mapData";
import winston from "../config/winston";

@controller("/api/book")
class BookController  implements interfaces.Controller {

    constructor( @inject("BookService") private bookService: BookService ) {
    }

    @httpGet("/")
    private  list(@request() req: express.Request, @response() res: express.Response) {
        try {
            senToClient(201, this.bookService.findAll(), res)
        } catch (err) {
            winston.error(` [${__filename}] get list book error: ${err}`);
            handleError({statusCode: 400, message: err}, res);
        }
    }
    @httpPost("/")
    private async create(@request() req: express.Request, @response() res: express.Response) {
        try {
            await this.bookService.addBook(req.body);
            senToClient(201 ,req.body, res);
        } catch (err) {
            winston.error(` [${__filename}] create book error: ${err}`);
            handleError({statusCode: 400, message: err}, res);
        }
    }
    @httpPut("/")
    private async update(@request() req: express.Request, @response() res: express.Response) {
        try {
            await this.bookService.update(req.body);
            senToClient(201 ,req.body, res);
        } catch (err) {
            winston.error(` [${__filename}] update book error: ${err}`);
            handleError({statusCode: 400, message: err}, res);
        }
    }
    @httpDelete("/")
    private async delete(@request() req: express.Request, @response() res: express.Response) {
        try {
            await this.bookService.delete(req.body);
            senToClient(201 ,req.body._id, res);
        } catch (err) {
            winston.error(` [${__filename}] delete book error: ${err}`);
            handleError({statusCode: 400, message: err}, res);
        }
    }
}
export default BookController;