import jwt    from "jsonwebtoken";

export function authenticate(req, res, next){
    // check header for the token
    var token = req.headers['access-token'];
    // decode token
    if (token) {
      // verifies secret and checks if the token is expired
      try {
        var decoded = jwt.verify(token, 'heymynameismohamedaymen');
        next();
      } catch(err) {
        return res.json({ message: 'invalid token' });    
      }
    } else {
      // if there is no token  
      res.send({ 
          message: 'No token provided.' 
      });
    }
  }

  export function createdToken(){
    const payload = {
        check:  true
      };
      return jwt.sign(payload, 'heymynameismohamedaymen', {
        expiresIn: 1440 // expires in 24 hours

  });
  }