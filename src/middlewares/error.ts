import * as express from 'express';


export function handleError(error: any, response: express.Response){
    const statusCode = error.statusCode;
    let  errMsg = '';
    if(error.message.errmsg){
        errMsg = error.message.errmsg
    }else errMsg = error.message
    response.send({  
        result : statusCode,
        message : errMsg
    })
  
}