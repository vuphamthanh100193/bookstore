import * as mongoose from 'mongoose';
// import uuidv1 from 'uuid/v1';



import { IBook } from '../interfaces/IBook';

interface IBookModel extends IBook, mongoose.Document{}

var bookSchema = new mongoose.Schema({

  title: {
    type: String,
    required: true,
    maxlength:30,
    unique: true,
  },
  category: {
    type:String,
    enum: ['drama', 'comedy', 'sport'],
    required: true,
  },
  description: {
    type: String,
    required: true
  }
})

var Book = mongoose.model<IBookModel>("Book", bookSchema);


export default Book;