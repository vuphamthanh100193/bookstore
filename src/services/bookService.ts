import Book from "../models/IBookModel";
import to from "await-to-js";
import { injectable, inject } from "inversify";
import { IBookServices } from '../interfaces/IBookService';
import { IBook } from '../interfaces/IBook';


@injectable()
class BookService implements IBookServices {
    public async findAll() {
        const [err, data] = await to(Book.find().exec());
        if (err) {
            throw err;
        }
        return data;
    }

    public async addBook(body) {
        let { title, category, description } = body;
        let book = new Book({ title, category, description });
        const [err, data] = await to(book.save());
        if (err) {
            throw err;
        }
        return data;
    }

    public async update(body) {
        let { _id, title, category, description } = body;
        const [err, data] = await to(Book.findOneAndUpdate({_id}, {title, category, description}, {
            new: true
          }).exec());
        if (err) {
            throw err;
        }
        return data;
    }

    public async delete(_id) {
        const [err, data] = await to(Book.findOneAndDelete({_id},).exec());
        if (err) {
            throw err;
        }
        return data;
    }
}

export { BookService };