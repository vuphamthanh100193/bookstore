import hello from '../controller/hello';
import  App  from '../../app';
const chai = require("chai");
const chaiHttp = require("chai-http");
import { CONFIG } from '../../src/config';
import "reflect-metadata"
import { injectable, Container } from "inversify";
import * as inversify from "inversify";
import "../../src/controller/bookController"
chai.use(chaiHttp);
const expect = chai.expect;
// if you used the '@types/mocha' method to install mocha type definitions, uncomment the following line
// import 'mocha';
const app = new App(CONFIG.PORT , CONFIG.DB_STRING);

describe('Hello function', () => {
//   it("welcomes user to the api", done => {
//     return chai.request(app).get('/getToken')
//       .then(res => {
//         expect(res).to.have.status(200);
//       })
// });
it("adds 2 numbers", done => {
  chai
    .request(app)
    .post("/getToken")
    .send({ "username": "admin", "password": 123 })
    .end((err, res) => {
      expect(res).to.have.status(200);
      expect(res.body.status).to.equals("success");
      done();
    });
});
})
