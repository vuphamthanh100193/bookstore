import 'dotenv/config';

export var CONFIG = {
    PORT: parseInt(`${process.env.PORT}`, 10) || 4000,
    DB_STRING: process.env.DB_STRING,
}
