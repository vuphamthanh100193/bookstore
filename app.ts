import express from 'express';
import bodyParse from 'body-parser';
import {Container} from 'inversify';
import {InversifyExpressServer} from 'inversify-express-utils';
import "./src/controller/bookController";
import * as bodyParser from 'body-parser';
import {getDatabaseClient} from './src/db/db_client';
import cors from 'cors';
import "reflect-metadata";
import './src/controller/bookController';
import {BookService} from "./src/services/bookService";
import { authenticate, createdToken } from "./src/middlewares/authenticate"

class App {
    public app: express.Application;
    public port: Number;
    public db_string: string;
    

    constructor(port: Number, db_string: string) {
        this.app = express();
        this.port = port;
        this.db_string = db_string;

        this.initializeMiddleware();
        this.connectToDB();
        this.listen();
    }

    private initializeMiddleware() {
        this.app.use(cors());
    }

    private async connectToDB() {
        await getDatabaseClient(this.db_string);

    }
    private getToken(app){
        app.post('/getToken',(req,res)=>{
            if(req.body.username==="admin"){
                if(req.body.password===123){
                     //if eveything is okey let's create our token 
                     let token = createdToken();
                  res.json({
                    message: 'authentication done ',
                    token: token
                  });
                }else{
                    res.json({message:"please check your password !"})
                }
            }else{
                res.json({message:"user not found !"})
            }
        })
    }

    public listen() {
        
    // set up container
        let container = new Container();
    // set up bindings
        container.bind<BookService>('BookService').to(BookService);
    // create server
        let server = new InversifyExpressServer(container);
        server.setConfig((app) => {
            // add body parser
            app.use(bodyParser.urlencoded({
                extended: true
            }));
            app.use(bodyParser.json());
            app.use('/api', (req, res, next)        => authenticate(req, res, next));
        });
        let app = server.build();
        this.getToken(app);
        app.listen(this.port, () => {
            console.info(`App is running on port ${this.port}`)           
        })
      
    }
}

export default App;
