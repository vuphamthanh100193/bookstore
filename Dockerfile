FROM node:10.16.3
ENV NODE_ENV production
RUN mkdir -p /home/node/app
WORKDIR /home/node/app

COPY package.json /home/node/app

RUN npm install

RUN npm install -g typescript

COPY . .
EXPOSE 4000
CMD ["npm", "run", "server"]